occurs_free_in(X, v(X)).

occurs_free_in(X, l(Y,T)) :-
		X \== Y,
		occurs_free_in(X, T).
	 
occurs_free_in(X, a(T1,T2)) :-
		occurs_free_in(X, T1)  ;
		occurs_free_in(X, T2).

/*
a(a(l(f, l(x, a(v(f), v(x)))), v(a)), v(b)).
a. Write a predicate eta(T) which succeeds only if a lambda-term T is an eta-redex.
b. Write a predicate beta(T) which succeeds only if a lambda-term T is a beta-redex.
c. Write a predicate normal(T) which succeed only if a lambda-term T is in normal form.
*/
/*
application = a( T, T)
abtraction = l (x, T)
variable = v (a)
*/

/* eta(T) :- occurs_free_in(X, T).	*/
/*eta( v(_)) :- fail. 

l(x,a(v(f),v(x)))
 */

eta(l(X,a(T,v(X)))):- \+occurs_free_in(X, T);eta(T).
eta(a(T1,T2)):- eta(T1);eta(T2).

/*eta(l(X,a(v(f),v(X)))):- occurs_free_in(X, v(f)).  */
/* ,eta(v(f)) .*/
/*eta( a( T, T)) 		:- eta(T) ; eta(T).*/

/* a term is normal term if it does not contain any beta and eta redex */
normal(T):- \+beta(T);\+eta(T).



