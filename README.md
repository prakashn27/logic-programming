#Logic Programming with Prolog #

This is the programming assignment given in CSE 505 as a part of academic project from University at buffalo. This material is solely provided for you understanding. 

####Question
1. Suppose we represent a lambda-term by Prolog constructors v, l, a, for variable, abstraction, and 
application respectively. For example, a lambda term ((λf.λx.(f x) a) b) would be represented by the
Prolog term 
a(a(l(f, l(x, a(v(f), f(x)))), v(a)), v(b)).
a. Write a predicate eta(T) which succeeds only if a lambda-term T is an eta-redex.
b. Write a predicate beta(T) which succeeds only if a lambda-term T is a beta-redex.
c. Write a predicate normal(T) which succeed only if a lambda-term T is in normal form.
Create a file called lambda.pl containing the above the three predicates as well as any other helper 
predicates that they use.
2. In class we examined the Prolog predicate ‘path’ for testing whether there is a path between a 
given pair of nodes in a directed graph:
path(From, To) :- edge(From, To).
path(From, To) :- edge(From, Z), path(Z, To).
The above predicate has one crucial defect: it could go into an infinite loop when there is a cycle in 
the graph represented by the ‘edge’ facts. Remedy this defect by defining path2(Start, End, Path) 
which, given the Start and End nodes, determines in Path a directed path in the graph without 
getting caught in an infinite loop. By repeated backtracking, path2 should yield (in Path) every path
between Start and End without repeating nodes in a cycle.
Create a file called path.pl containing the definition of the path2 predicate as well as any other 
helper predicates that it uses.
