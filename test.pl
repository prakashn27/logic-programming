path8(S,S,[], Rec).
path8(N,S, [N,S|T], Rec) :- \+is_contains(D, Rec),edge(N,S), path8(S,S,[],Rec).
path8(D,S, [D|T], Rec) :- \+is_contains(D, Rec), edge(D, L), path8(L, S, T,[D | Rec]).
/* one element is missing */

/* got four outpus : only two lines */ 
path8(S,S,[], Rec).
path8(S,S, [S], Rec) :- path8(S,S, [], Rec).
path8(N,S, [N,S], Rec) :- \+is_contains(D, Rec),edge(N,S), path8(S,S,[S],Rec).
path8(D,S, [D|T], Rec) :- \+is_contains(D, Rec), edge(D, L), path8(L, S, T,[D | Rec]).