is_contains(X,[X|_]). 
is_contains(X,[_|T]):- is_contains(X,T). 


/* test case path(nyc,sfo).	*/
edge(nyc,chicago). 
edge(nyc,dallas). 
edge(chicago, la).
edge(dallas, la).
edge(la, sfo).
edge(sfo,la).
edge(la, dallas).
edge(dallas, nyc). 

path(N,S,[H|T]) :- path8(N,S,[H|T], []).

path8(S,S,[], Rec).
path8(S,S, [S], Rec) :- path8(S,S, [], Rec).
path8(N,S, [N,S], Rec) :- \+is_contains(D, Rec),edge(N,S), path8(S,S,[S],Rec).
path8(D,S, [D|T], Rec) :- \+is_contains(D, Rec), edge(D, L), path8(L, S, T,[D | Rec]).


/* if Z is already in path then looping so it fails */