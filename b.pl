/*  beta-redex  */
/*	a( l(x, v(a) ), v(b) )
beta(a(l(x,v(a)),v(b))).
*/
/*
application = a( T, T)
abtraction = l (x, T)
variable = v (a)
*/

beta(a(l(X,T),_)).	/* simple case of application with abstraction */
beta(a(T1,T2)):-beta(T1);beta(T2).	/* case of application */
beta(l(X,T)):-beta(T).		/* case of abstraction*/


/*
base case : beta(a(l(x,v(a)),v(b))).
AA :	beta(a(a(l(x,v(a)),v(b)),a(l(x,v(a)),v(b)))).
ABS:	beta(l(x,a(l(x,v(a)),v(b)))).
*/