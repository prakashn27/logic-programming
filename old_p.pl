/* HELPER FUNCIONS */
/* used to search the element X in the list and return true if it is found in list */
/* @args: element_to_be_searched
		  list_name
*/
is_contains(X,[X|_]). 
is_contains(X,[_|T]):- is_contains(X,T). 

/* append function of list */
app([], L2, L2).
app([H|T], L2, [H|L3])  :- app(T,L2,L3).

/* test case path(nyc,sfo).	*/
edge(nyc,chicago). 
edge(nyc,dallas). 
edge(chicago, la).
edge(dallas, la).
edge(la, sfo).
edge(sfo,la).
edge(la, dallas).
edge(dallas, nyc). 

/* REAL FUNCTION */
/* loop termination */
/* path(X,Y) :- edge(X,Y).
path(X,Y) :- edge(X,Z),  path2(Z,Y,[X]). */

path(X,Y, [H|T]) :- path2(X, Y, [H|T], []). 	/*empty list checks for the looping sequence */
/* if Z is already in path then looping so it fails */
path2(X,X,Path, _).
path2(D, S, [D|S], Recpath):- \+is_contains(D, Recpath), edge(D, S). /* path2(S, S, T, [D | RecPath]).*/
path2(D,S, [D|T], Recpath) :- \+is_contains(D, Recpath), edge(D, L), path2(L, S, T, [D | RecPath]).

path3(S,S, []).
path3(S,S, [S|T]) :- path3(S,S, T).
path3(L,S,[L|T]) :- edge(L,S), path3(S, S, T).
path3(N, S, [N|T]) :-  edge(N,C), path3(C,S,T).

path4(S,S,[]).
path4(L,S,[]) :- path4(S,S, []).
path4(L,S, [H|T]) :- edge(L,S), path4(L,S,T).	/* edge present */
path4(N,S,[N|T]) :- edge(N,C), path4(C,S,T).

path8(L,S, [L|T]) :- edge(L,S).
path8(L,S, [L|T]) :- edge(X,Z),  path9(Z,Y,T, [L]).

/* if Z is already in path then looping so it fails */
path9(X,X,Path).
path9(D,S,Path) :- \+is_contains(D, Path), edge(D, L), path9(L, S, [D | Path]).